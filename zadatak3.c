#include <stdio.h>

#define BACK_MASK 0x0001
#define FRONT_MASK 0x8000

unsigned short get_bit(unsigned short, int);
void set_bit(unsigned short *, int);
void clear_bit(unsigned short *, int);
void flip_bit(unsigned short *, int);

void print_binary(unsigned short);

int main() {
  
    printf("%hd\n", get_bit(3, 0));
  
    unsigned short a = 2;
    
    print_binary(a);
    set_bit(&a, 2);
    print_binary(a);
    clear_bit(&a, 2);
    print_binary(a);
    flip_bit(&a, 0);   
    print_binary(a);
 
    return 0;
}

unsigned short get_bit(unsigned short number, int position) {
    return ((number >>= position) & BACK_MASK) != 0;
}

void set_bit(unsigned short *pnumber, int position) {
    unsigned short mask = BACK_MASK;
    mask <<= position;
    
    (*pnumber) |= mask;
}

void clear_bit(unsigned short *pnumber, int position) {
    unsigned short mask = BACK_MASK;
    mask <<= position;
    
    (*pnumber) &= ~mask;
}

void flip_bit(unsigned short *pnumber, int position) {
    get_bit(*pnumber, position) ? clear_bit(pnumber, position) : set_bit(pnumber, position);
}

void print_binary(unsigned short number) {
    int i;
    int size_in_bits = sizeof(number) * 8;
    
    for(i = 1;i <= size_in_bits;i++) {
	printf("%hu", (number & FRONT_MASK) != 0);
	number <<= 1;
	
	if(i % 4 == 0) {
	    printf(" ");
	}
    }
    
    printf("\n");
}
