#include <stdio.h>
#include <stdlib.h>

typedef struct cvor_st {
    char inf;
    struct cvor_st *sledeci;
} CVOR;

void init(CVOR **);
CVOR *napravi_cvor(char);
void dodaj_cvor(CVOR **, CVOR *);
void listanje_liste(CVOR *);
void brisanje_liste(CVOR **);
void obrisi_cvor(CVOR **, char);

int main() {
    CVOR *glava;

    init(&glava);

    dodaj_cvor(&glava, napravi_cvor('a'));
    dodaj_cvor(&glava, napravi_cvor('b'));
    dodaj_cvor(&glava, napravi_cvor('c'));

    listanje_liste(glava);
    obrisi_cvor(&glava, 'b');
    listanje_liste(glava);

    printf("Sadrzaj pokazivacke promenljive 'glava' pre brisanja liste: %p\n", glava);
    brisanje_liste(&glava);
    printf("Sadrzaj pokazivacke promenljive 'glava' posle brisanja liste: %p\n", glava);
}

void init(CVOR **glava) {
    *glava = NULL;
}

CVOR *napravi_cvor(char c) {
    CVOR *novi = (CVOR *)malloc(sizeof(CVOR));

    novi->inf = c;
    novi->sledeci = NULL;

    return novi;
}

/*
    U ovom slucaju, novi cvor liste dodaje se na kraj.

    Varijante koje jos postoje:
        - dodavanje na pocetak liste
        - dodavanje negde izmedju pocetka/kraja
*/
void dodaj_cvor(CVOR **pglava, CVOR *novi) {
    if(*pglava == NULL) {
        *pglava = novi;
    } else {
        CVOR *tek = *pglava;

        while(tek->sledeci != NULL) {
            tek = tek->sledeci;
        }

        tek->sledeci = novi;
    }
}

void listanje_liste(CVOR *glava) {
    CVOR *tek = glava;

    printf("[");
    while(tek != NULL) {
        if(tek != glava) {
            printf(", ");
        }
        printf("%c", tek->inf);
        tek = tek->sledeci;
    }
    printf("]\n");
}

void brisanje_liste(CVOR **pglava) {
    CVOR *tmp = *pglava;

    while(*pglava != NULL) {
        tmp = *pglava;            // postavi privremeni pokazivac na pocetak liste
        *pglava = tmp->sledeci;   // pomeri glavu (pocetak liste) na sledeci cvor
        tmp->sledeci = NULL;      // ukini vezu bivseg pocetka liste sa ostatkom
        free(tmp);                // oslobodi memoriju koji je zauzimao bivsi prvi cvor liste
    }
}

void obrisi_cvor(CVOR **pglava, char c) {
    CVOR *tek = *pglava, *pret;

    while((tek != NULL) && (tek->inf != c)) {
        pret = tek;
        tek = tek->sledeci;
    }

    if(tek == *pglava) {
        *pglava = tek->sledeci;
        tek->sledeci = NULL;
        free(tek);
    } else {
        pret->sledeci = tek->sledeci;
        tek->sledeci = NULL;
        free(tek); 
    }
}

