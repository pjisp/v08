#include <stdio.h>

#define BINARY_MASK 0x8000

void print_binary(unsigned short);

int main() {
    unsigned short num1, num2;
    
    printf("Uneti prvi broj: ");
    scanf("%hu", &num1);
    printf("Uneti drugi broj: ");
    scanf("%hu", &num2);
    
    printf("Binarni ispis brojeva:\n");
    print_binary(num1);
    print_binary(num2);

    printf("\nKomplementi brojeva:\n");
    print_binary(~num1);
    print_binary(~num2);

    printf("\nLogicko I:\n");
    print_binary(num1 & num2);
    
    printf("\nLogicko ILI:\n");
    print_binary(num1 | num2);
    
    printf("\nEkskluzivno logicko ILI:\n");
    print_binary(num1 ^ num2);
    
    return 0;
}

void print_binary(unsigned short number) {
    int i;
    int size_in_bits = sizeof(number) * 8;
    
    for(i = 1;i <= size_in_bits;i++) {
	printf("%hu", (number & BINARY_MASK) != 0);
	number <<= 1;
	
	if(i % 4 == 0) {
	    printf(" ");
	}
    }
    
    printf("\n");
}